package main

import (
	"bufio"
	"flag"
	"log"
	"math/rand"
	"os"
	"time"

	"gitlab.com/liftedkilt/manta-backup-source/client"
	"gitlab.com/liftedkilt/manta-backup/manta"
)

var (
	dryRun      bool
	logFile     string
	mountsHost  string
	mountsKey   string
	sourcesFile string
)

func init() {
	flag.StringVar(&mountsHost, "host", "", "host:port of server running manta-backup-source")
	flag.StringVar(&mountsKey, "key", "", "auth key for manta-backup-source")
	flag.StringVar(&sourcesFile, "file", "", "path to sources file")
	flag.StringVar(&logFile, "log", "", "file to write logs to")
	flag.BoolVar(&dryRun, "dry-run", false, "Display values only")
	flag.Parse()
}

func main() {
	if logFile != "" {
		f, err := os.OpenFile(logFile, os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0644)
		if err != nil {
			log.Fatal(err)
		}

		defer f.Close()
		log.SetOutput(f)
	}

	rand.Seed(time.Now().UnixNano())

	// set config
	if len(os.Args) < 2 {
		log.Fatalln("Provide a path to a file containing mounts to sync")
	}

	mounts, err := client.GetMounts(mountsHost, mountsKey)
	if err != nil {
		log.Fatalln(err)
	}

	sources, err := readLines(sourcesFile)
	if err != nil {
		log.Fatalln(err)
	}

	if !dryRun {
		manta.Sync(mounts, sources)
	} else {
		log.Println("Mounts Host:", mountsHost)
		log.Println("Mounts Key:", mountsKey)
		log.Println("===Sources===")
		for _, source := range sources {
			log.Println(source)
		}
		log.Println("===Mounts===")
		for _, mount := range mounts {
			log.Println(mount)
		}
	}

}

// Takes a path to a file, returns the contents of the file as a slice of strings and an error object
func readLines(path string) ([]string, error) {
	file, err := os.Open(path)
	if err != nil {
		log.Fatalln(err)
	}

	defer file.Close()

	var lines []string
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}
	return lines, scanner.Err()
}
