#!/bin/bash

PROJPATH=$GOPATH/src/gitlab.com/liftedkilt/manta-backup


function cleanBin() {
    rm -rf $PROJPATH/bin/*
}

function cleanAll() {
    cleanBin
}

function buildServer() {
    go build -o $PROJPATH/bin/manta-backup $PROJPATH/main.go
}

function buildAll() {
    buildServer
}

function main() {
    if [ "$1" == "build" ]; then
        if [ "$2" == "server" ]; then
            buildServer
        elif [ $2 == "all" ]; then
            buildAll
        fi
    elif [ "$1" == "clean" ]; then
        if [ "$2" == "bin" ]; then
            cleanBin
        elif [ $2 == "all" ]; then
            cleanAll
        fi
    elif [ "$1" == "all" ]; then
        cleanAll
        buildAll
    fi
}

main "$@"