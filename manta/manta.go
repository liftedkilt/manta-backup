package manta

import (
	"log"
	"math/rand"
	"os/exec"
	"sync"
)

var (
	mantaCompleted int // number of manta jobs that completed
	mantaErrored   int // number of manta jobs that ended in errors
	mantaStarted   int // number of manta jobs started
	mfsCompleted   int // number of mfs jobs that completed
	mfsErrored     int // number of mfs jobs that ended in errors
	mfsStarted     int // number of mfs jobs started
)

// Worker function to perform the rsync from the given source domain and MFS mount
func mfsSync(source, mount string) (result string, err error) {
	rsyncCmd := "rsync -rltzuv --exclude 'mirrors.lax.liferay.com' --safe-links --delete --ignore-errors rsync://" + source + "/mounts/" + mount + "/ /data/" + mount + "/ >> /var/log/backup/" + mount + ".log"

	bytes, err := exec.Command("sh", "-c", rsyncCmd).Output()
	result = string(bytes)

	return
}

// Worker function to perform the push to manta local copy of the MFS mount
func mantaSync(mount string) (result string, err error) {
	mantaCmd := "/usr/local/bin/manta-sync -p40 -c1 -d /data/" + mount + "/ ~~/stor/" + mount + " >> /var/log/manta-sync/" + mount + ".log"

	bytes, err := exec.Command("sh", "-c", mantaCmd).Output()
	result = string(bytes)

	return
}

// Concurrent function that calls the MFS worker and returns the result to a channel after logging the output
func syncMFS(source, mount string, completed chan string, wg *sync.WaitGroup) {
	mfsStarted++
	log.Println("MFS - Starting", mount, "(", source, ")")
	result, err := mfsSync(source, mount)

	if err != nil {
		mfsErrored++
		log.Println("MFS-ERROR:", mount, "(", source, ")", err)
	} else {
		mfsCompleted++
		log.Println("MFS - Finished", mount, "(", source, ")", result)
		completed <- mount
	}

	wg.Done()
}

// Concurrent function that calls the Manta worker and logs the output
func syncManta(mount string, wg *sync.WaitGroup) {
	mantaStarted++
	log.Println("Manta - Starting", mount)

	result, err := mantaSync(mount)

	if err != nil {
		mantaErrored++
		log.Println("MANTA-ERROR:", mount, err)
	} else {
		mantaCompleted++
		log.Println("Manta - Finished", mount, result)
	}

	wg.Done()
}

// Sync is a concurrent function that dispatches goroutines for each MFS mount
func Sync(mounts, sources []string) {
	var mfsGroup sync.WaitGroup
	var mantaGroup sync.WaitGroup

	// Channel to store the mounts that have finished the rsync from MFS
	completed := make(chan string)

	// Dispatch a goroutine to wait for both batches of workers to return and then close the channel
	go func() {
		mfsGroup.Wait()
		mantaGroup.Wait()
		close(completed)
	}()

	// Dispatch a MFS worker to begin the rsync for each
	for _, mount := range mounts {
		source := pickSource(sources)
		go syncMFS(source, mount, completed, &mfsGroup)
		mfsGroup.Add(1)
	}

	// Monitor the completed channel, and dispatch a worker to push to manta once each rsync worker is done
	for mount := range completed {
		mantaGroup.Add(1)

		log.Println("[MFS] started:", mfsStarted, "completed:", mfsCompleted, "errored:", mfsErrored)
		log.Println("[Manta] started:", mantaStarted, "completed:", mantaCompleted, "errored:", mantaErrored)

		go syncManta(mount, &mantaGroup)
	}
}

// Grab a random source from the given slice
func pickSource(h []string) string {
	return h[rand.Intn(len(h))]
}
